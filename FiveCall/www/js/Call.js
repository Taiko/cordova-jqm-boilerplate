//While adding a new class, don't forget :
//Import the file in index.js, by doing impt(classname);
//Create a new instance like this var obj = new Classname();
//Then call it's methods : obj.function1(); etc...
function Call(/*params if any*/) {  
	var self = this;
	//first, we assign self
    
    self.init = function(/*params if any*/){
        //This class's constructor
	 	
    }
    
    //make a call
    self.call = function(myNumber,dirNumber ) {

		alert( myNumber+" is Calling "+dirNumber+".........");
		
     	$.post("http://yourOwnTwilioServerAdressGoesHere.com/call.php?n1="+dirNumber+"&n2="+myNumber);	 
    } 
    
    //The call of the constructor should be the last one
    self.init(/*params if any*/);
}