//In case this class is a controller, uncomment this
$(document).on("pagebeforeshow","#dialer",function(){
  new DialerController();
});


//While adding a new class, don't forget :
//Import the file in index.js, by doing impt(classname);
//Create a new instance like this var obj = new Classname();
//Then call it's methods : obj.function1(); etc...
function DialerController(/*params if any*/) { 
	var self = this;
	//first, we assign self
	
	self.myNumber = window.localStorage.getItem("myPhoneNumber");
	
    self.init = function(/*params if any*/){
        //This class's constructor
	 	$("#one").off().on('click',function(){
			self.insertNumber(1);
		});
		$("#two").off().on('click',function(){
			self.insertNumber(2);
		});
		$("#three").off().on('click',function(){
			self.insertNumber(3);
		});
		$("#four").off().on('click',function(){
			self.insertNumber(4);
		});
		$("#five").off().on('click',function(){
			self.insertNumber(5);
		});
		$("#six").off().on('click',function(){
			self.insertNumber(6);
		});
		$("#seven").off().on('click',function(){
			self.insertNumber(7);
		});
		$("#eight").off().on('click',function(){
			self.insertNumber(8);
		});
		$("#nine").off().on('click',function(){
			self.insertNumber(9);
		});
		$("#zero").off().on('click',function(){
			self.insertNumber(0);
		});
		$("#sign1").off().on('click',function(){
			self.insertNumber("*");
		});
		$("#sign2").off().on('click',function(){
			self.insertNumber("+");
		});
		$("#call").off().on('click',function(){
			self.callNumber();
		});
		
		
    }
    
    //insert number
    self.insertNumber = function(num) {
		$("#Number").val($("#Number").val()+num);
     
    }
	
	//make a  call
    self.callNumber = function() {
    
			var call = new Call();
			
			var dirNumber = $("#Number").val();
			
			call.call(self.myNumber,dirNumber);
    }
    
    
    //The call of the constructor should be the last one
    self.init(/*params if any*/);
}