//In case this class is a controller, uncomment this
$(document).on("pagebeforeshow","#settings",function(){
  new SettingsController();
});


//While adding a new class, don't forget :
//Import the file in index.js, by doing impt(classname);
//Create a new instance like this var obj = new Classname();
//Then call it's methods : obj.function1(); etc...
function SettingsController(/*params if any*/) {
    var self = this;
    //first, we assign self
    
    self.init = function(/*params if any*/){
    
        //This class's constructor
		$("#myPhoneNumber").val(window.localStorage.getItem("myPhoneNumber"));
		
        $("#save").off().on('click',function(){
        
			self.save();
		
		})
    }
    
    
    //save your number
    self.save = function() {
		if(window.localStorage.myPhoneNumber) window.localStorage.removeItem("myPhoneNumber");
		
		window.localStorage.setItem("myPhoneNumber", $("#myPhoneNumber").val()); 
		
		$("#savealert").html("save success");
    }
    

    
    //The call of the constructor should be the last one
    self.init(/*params if any*/);
}