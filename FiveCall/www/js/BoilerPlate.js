/*
//If this class is a Controller, we register the pagebeforeshow
$(document).on("pagebeforeshow","#dialer",function(){
  new DialerController();
  new I18n();
});
*/


//While adding a new class, don't forget :
//Import the file in index.js, by doing impt(classname);
//Create a new instance like this var obj = new Classname();
//Then call it's methods : obj.function1(); etc...
function BoilerPlate(/*params if any*/) {
    var self={}; self.publ = this;
    //First, we assign self and the public interface self.publ
    
    
    
    //Private variables (no public variables, is a bad practice)
    
    /*private*/ self.variableName = "a private variable, call it simply like this : alert(self.variableName)";
    
    
    
    //Constructor
    
    /*constructor*/ self.init = function(/*params if any*/){
        //This class's constructor
        
    }
    
    
    
    //Public functions (there should be as little public functions as possible)
    
    //To call this function, call self.publ.functionName1(); (inside the object)
    //Or objectName.functionName1(); (outside the object)
    /*public*/ self.publ.functionName1 = function() {
        
        alert(self.variableName);
    }
    
    
    //
    /*public*/ self.publ.functionName2 = function(param) {
    
        self.functionName3(param);
    }

    
    
    //Private functions
    
    //To call this function (inside the object only), just call self.functionName3();
    /*private*/ self.functionName3 = function(param) {
        
        alert(param);
    }
    

    //
    /*private*/ self.functionName4 = function(param) {
        
        alert(param);
    }

    
    
    //The call of the constructor should be the last line
    self.init(/*params if any*/);
}

//This is a static function, it can be called outside of an instance
//Like BoilerPlate.staticFunction();
///*static*/ BoilerPlate.staticFunction = function(message) {
//    alert(message); 
//}