// This function is used to import another class
function impt(file){
    document.write('<script src="js/'+file+'.js" type="text/javascript"></script>');
}

//Import all classes here
impt("SettingsController");
impt("DialerController");
impt("Call");

var app = {
    // Application Constructor
    initialize: function() {
    	document.addEventListener('deviceready', this.onDeviceReady, false);
        //This is our main for non-device (e.g jquery mobile) related tasks ! Do everything that needs to be done on startup here !
     
    },

    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //This is our main for all device (e.g cordova) related tasks ! Do everything that needs to be done on startup here !
        
    }
};
